# Handling Deployment failures

Sometimes a deploy pipeline fails. We should always be aware of these failures
through notifications coming in through Slack on `#announcements`. Note you will
only see one failure notice per pipeline, so keep an eye on a pipeline after a
retry.

If you cannot resolve the failure within 5 minutes, follow these steps for help:
1. For QA failures, follow the [runbook for resolving qa failures](https://gitlab.com/gitlab-org/release/docs/-/blob/master/runbooks/resolving-qa-failures.md)
1. For problems needing Dev-escalation/developer or Reliability investigation or action please [report an incident](https://about.gitlab.com/handbook/engineering/infrastructure/incident-management/#report-an-incident-via-slack). Make sure to apply the correct [availability severity](https://about.gitlab.com/handbook/engineering/infrastructure/incident-management/#incident-severity). 
1. Once resolved please add the `Deploys-block-gprd::*` and `Deploys-block-gstg::*` labels to record the delay so we can improve things

Anyone can stop a deployment by following the steps in
[deployment blockers][].

## Failure Cases

Possible failures, and ways to respond for the individual deployment jobs:

* **Missing branches or packages** -- The package pipeline on the Dev instance has failed, or is running for longer than expected. You can try to debug yourself from https://dev.gitlab.org/gitlab/omnibus-gitlab/-/pipelines or reach out to `#g_distribution` to ask for support.
* **Prepare job** - This job ensures that servers are in the appropriate state
  in our load balancers and that the version of GitLab is the same across the
  fleet.  Action must be taken manually in order to remediate this type of
  failure. Check for guidance in the job log file.
* **Fleet Deploy** - This can fail for a myriad of reasons.  Each failure
  should be investigated to determine the appropriate course of action
  accompanied with an issue where appropriate.  A retry is usually safe to
  attempt but use your judgement based on the error reported.
* **QA** - QA jobs run following deployment completion. If any QA tasks fail we
  should assume something has regressed with the deployment. Use the [runbook to
  resolve the failure](https://gitlab.com/gitlab-org/release/docs/-/blob/master/runbooks/resolving-qa-failures.md).
* **Post Deploy Migrations** - See [post-deployment migration failures][] below.

[post-deployment migration failures]: #post-deployment-migration-failures

## Troubleshooting

### Post-deployment migration failures

A failing post-deployment migration should not be retried without investigation,
because data could be in an unstable state. Treat this type of failure as a
[deployment blocker][deployment blockers] and work with the SRE on call and Dev
Escalation as needed to work out next steps. When creating incident issues,
don't worry about not having all the details yet; a link to a failing pipeline
with some basic information is enough to begin.

Once the issue is created, determine what the next steps are:

* Do we need SREs to help look into the issue?
* Do we need database experts to help us out?
* Does it require code changes?
* Will it potentially impact self-managed users in the next release?

Make sure to add any additional information to the incident, such as links to
the merge request that introduced the migration. Note that as a release manager
**your primary task at this point is to coordinate the effort to resolve the
incident**, rather than trying to resolve it yourself. This helps balance the
workload, instead of one person having to do all the work.

If the incident happens at the end of your shift and there is no immediate need
to resolve it, make this clear in both the issue and the appropriate Slack
channels, and inform the next release manager about the state of things. This
ensures the next release manager can take over the work when they begin their
shift.

### Production deploy failed, but okay to leave in place

In situations where production deploys fail for any reason (such as post-deploy
migration failures), but it is deemed safe to leave production as-is, we need to
ensure that we don't prevent future deploys from being blocked.  Run the
following from the `deploy-tooling` repository:

```
CURRENT_DEPLOY_ENVIRONMENT=<env> DEPLOY_VERSION=<version> ./bin/set-version
./bin/set-omnibus-updates gstg --enable
```

This will configure the `<env>-omnibus-version` chef role to the appropriate
version and ensure installation is enabled of that version.  This happens
automatically during successful pipelines.

This is slated to become ChatOps commands: https://gitlab.com/gitlab-com/gl-infra/delivery/issues/524

### Prepare job discovered nodes in `DRAIN`

The prepre job (`<ENV>-prepare`) will fail if nodes are not in state `UP` or `MAINT`. Any other
state and the prepare job will hard fail noting which frontend server contains the
state, and which backend server is in this state. Unless there's known
maintenance happening there should not exist a situation where a server isn't in
either of the preferred states.

If a server is in state `DRAIN` a prior deploy may not have fully completed.
In this case, set the server into `MAINT` and retry.

This can be accomplished by following the [documentation for setting haproxy server state](https://gitlab.com/gitlab-com/runbooks/blob/master/howto/haproxy.md#set-server-state).
If you don't have access to the nodes, ask the SRE on call for GitLab Production to help you with this. You can find the engineer on call through ChatOps:

```bash
/chatops run oncall production
```

### Fleet Deploy

* `E: Could not get lock /var/lib/apt/lists/lock - open (11: Resource
  temporarily unavailable)`
  * This is common if there's a collision with chef running and the deploy
    trying to perform similar actions
  * Though we've tried to eliminate these issues as much as possible, hitting
    retry is usually the best method to allow the deploy to continue
* Timeout when setting haproxy state
  * This is common for our `pages` fleet when pages is starting up
  * [Pages service takes a long time to start up], hitting retry for this job
    helps.  The timeout is already very high on this particular task, if we hit
    the timeout again, we should open an issue to investigate further.
  * If this happens on a server unrelated to the `pages` service starting, this
    must be deeply investigated on the node that exhibited the failure

[gitlab issue tracker]: https://gitlab.com/gitlab-org/gitlab/issues
[Pages service takes a long time to start up]: https://gitlab.com/gitlab-org/gitlab-pages/issues/41
[deployment blockers]: https://about.gitlab.com/handbook/engineering/releases/#deployment-blockers
