# Overview

This document describes the process to follow if QA tests are failing.

QA smoke tests are run as part of the auto-deploy pipeline - this means they are run regularly and can be assumed as stable.


# Process Overview

Failing QA tests are always tracked using an issue in the [release tracker](https://gitlab.com/gitlab-org/release/tasks/-/issues) to give a record of the failure. This is a change away from incidents as part of https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/752
 
Quality are here to help us investigate and resolve any failures and maintain a schedule to show who to ping. All test results are posted in the #qa-<env> Slack channels.

# Process steps

1. Open an issue in the [release issue tracker](https://gitlab.com/gitlab-org/release/tasks/-/issues). Include the date, package name, as well as details of the failure
1. Apply the `release-blocker` label
1. Check the `qa-<env>` Slack channel, the failure may already be known and being worked on
1. To escalate simply ping the engineer listed in the [Quality on call schedule] and ask for assistance on the issue. The #quality Slack channel can also be used.  
1. Once tests are passing again update the issue with a summary of the failure. Apply the `deploys-blocked-gprd::*` and `deploys-blocked-gstg::*` labels and close the issue

# Quality on call  

* Quality maintain a schedule of engineers on call to assist us. See [their responsibilities](https://about.gitlab.com/handbook/engineering/quality/guidelines/#responsibility).

* [Quality on call schedule]

[Quality on call schedule]: https://about.gitlab.com/handbook/engineering/quality/quality-engineering/oncall-rotation/#schedule
